# Hub

**Hub** este o platformă simplă și eficientă cu care partajeze toate legăturile importante într-un singur loc.

Inspirat de funcționalitatea Linktree, Hub îmi oferă posibilitatea de a crea o pagină personalizată unde pot adăuga toate legăturile preferate, fie că sunt către rețelele de socializare, sit-uri personale, proiecte sau alte resurse online.
